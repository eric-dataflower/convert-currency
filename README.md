# Currency Converter #

This project implements a currency converter product, where a user can convert
amounts between currencies using a realtime exchange rate.

[http://fx.int.dataflower.com.au/](http://fx.int.dataflower.com.au/)

![Example Image](app.png "AUD to USD")

This project is implemented via Scrum and in sprints.

## Scrum Artifacts ##

- [Sprint 1: Product Backlog Board](https://trello.com/b/f7WgTxct/sprint-1-product-backlog-board)
- [Sprint 1: Tasks Board](https://trello.com/b/vLZEuQPG/sprint-1-tasks-board)
- [User Story & Acceptance Criteria](https://trello.com/c/Z0GSNX7D/)
- [Sprint 1: Planning](https://trello.com/c/8zQotmEa/8-planning-march-23)
- [Sprint 1: Review](https://trello.com/c/FX8Jw3Uj/14-review-march-27)
- [Sprint 1: Retrospective](https://trello.com/c/LVCVoHUE/15-retrospective-march-27)

## Scrum Roles ##

| Scrum Role    | Name               | Specialty             |
| ------------- | ------------------ | --------------------- |
| Product Owner | M                  | Product & UX Design   |
| Scrum Master  | Eric Man           | Software Development  |

## About ##

#### Why? ####
The purpose is to demonstrate technical and non-technical skill and experience
in a more intensive manner than a CV or an interview.

#### What's Interesting? ####
Here are some notable and implemented aspects:

- Project Management:
  - Built with Scrum / [Product Backlog](https://trello.com/b/f7WgTxct/) / [Sprint Board](https://trello.com/b/vLZEuQPG/)
- Architecture:
  - Backend architecture and integration to external services implemented using a [pattern described by Martin Fowler](https://martinfowler.com/articles/refactoring-external-service.html)
  - API layer implemented using [AppSync](https://aws.amazon.com/appsync/), with a [GraphQL schema](src/appsync/schema.graphql).
- Backend Programming:
  - [Documenting external service API request and response formats using Python type annotations.](src/convert/src/connections.py)
  - Python [MyPy](https://mypy.readthedocs.io/en/stable/) type annotations. See [src/convert/src/services.py](src/convert/src/services.py) for an example.
  - [Unit tests](https://gitlab.com/eric-dataflower/convert-currency/-/blob/master/src/convert/src/test_connections.py) of normal and exception paths.  
- Frontend Programming:
  - Created with [create-react-app](https://reactjs.org/docs/create-a-new-react-app.html)
  - Connects to AppSync GraphQL API using [Apollo](https://apollographql.com/client)
  - UI built with [Material UI](https://material-ui.com/)
  - Hosted on AWS S3, deployed using Terraform.
  - Domain DNS manually configured on Cloudflare.
- Infrastructure & Operations:
  - Setting up of SAML SSO access to [AWS Console](https://dev-4xo5okqj.au.auth0.com/samlp/jpIFDUsz4xSkOABrbs4nPyAour2lYy03) and aws-cli using [Auth0](https://auth0.com).
  - AWS Infrastructure configured using [Terraform code](src/convert/main.tf) rather than manually.
  - Setting up AWS roles, policies, GitLab CI access on Amazon using [Terraform code](https://gitlab.com/eric-dataflower/infra/-/blob/master/main.tf), rather than manually.
  - Deploying [backend as a AWS Lambda using Terraform](src/convert/main.tf).
  - [Continuous integration and deployment via GitLab](.gitlab-ci.yml).
  - Secrets such as API key encrypted using AWS KMS and [committed into repository](https://gitlab.com/eric-dataflower/convert-currency/-/blob/master/src/convert/main.tf#L22) with minimal security risk.

#### What next? ####

- By prefixing all AWS resources defined in Terraform using `terraform.workspace`, developers can stand up their own individual environments, their own set of Lambda's and API's using `terraform workspace select firstname_dev`.
- Using the same technique, we can configure GitLab CI to stand up a standalone environment for each branch, to run integration tests with, and then destroying the environment.
- Focus on doing git-flow more strictly.
- **Replace magic strings with constants.**
- Logging API errors into Sentry? Slack? Cloudwatch Alarm?
- ...Potential improvements to how Scrum was run (See [Sprint 1: Retrospective](https://trello.com/c/LVCVoHUE/15-retrospective-march-27)).


## Infrastructure & DevOps ##

- [Auth0](https://auth0.com) for developer user management.
- [AWS Console](https://dev-4xo5okqj.au.auth0.com/samlp/jpIFDUsz4xSkOABrbs4nPyAour2lYy03) login page.

The following infrastructure have been setup:

- [Auth0 SAML Single Sign-On](https://auth0.com/docs/integrations/aws/sso) integration with AWS SAML 2.0.
- [Auth0 Login](https://github.com/binxio/auth0-login) setup for authenticating using Auth0 to run [Terraform](http://terraform.io) deployment.
- An S3 bucket in `ap-southeast-2` named `dataflower-terraform-state`.
- A DynamoDB table in `ap-southeast-2` named `terraform-state-lock` with `LockID` as the primary key.

### Developer Setup ###

Following the below instructions will enable a developer to deploy the project
on their own machines using [Terraform](http://terraform.io).

1. Run `pipenv install --dev`.
2. Run
```
echo "$(cat << EOF
[DEFAULT]
idp_url=https://dev-4xo5okqj.au.auth0.com
client_id=xizd66t1dbcPRNJVF65VqBTBAL7RxqvT

[DATAFLOWER_Main]
idp_url=https://dev-4xo5okqj.au.auth0.com
client_id=xizd66t1dbcPRNJVF65VqBTBAL7RxqvT
aws_account=dataflower
aws_role=Administrator
aws_profile=DATAFLOWER_Main
EOF
)" > ~/.saml-login
```
3. Run
```
echo "$(cat << EOF
[DEFAULT]
dataflower=915432453250
EOF
)" > ~/.aws-accounts
```
4. Test using `saml-login aws-assume-role --show`.
5. Authenticate using `saml-login -c DATAFLOWER_Main aws-assume-role`.
6. In [src/convert](src/convert), run the following:
  - `AWS_PROFILE=DATAFLOWER_Main terraform workspace new int`
  - `AWS_PROFILE=DATAFLOWER_Main terraform init`

<strong>Note:</strong>

The [infra/bin/sandbox-aws](https://gitlab.com/eric-dataflower/infra/-/blob/master/bin/sandbox-aws) command available
for the purpose of executing commands in the Sandbox account outside of
Terraform. e.g. for encrypting and decrypting secrets using kms in the Sandbox
account.

### GitLab CI & Terraform Setup ###

[infra](https://gitlab.com/eric-dataflower/infra/) contains configures AWS users and roles for deploying
the project from Terminal, and from GitLab, via Terraform.

Permissions are added to the `Deploy` IAM role as they become required for
deployment. i.e. when running a deployment and getting an `AccessDenied`
message. See `aws_iam_policy_attachment`'s attached to
`aws_iam_role.deploy_sandbox` in [infra/main.tf](https://gitlab.com/eric-dataflower/infra/-/blob/master/main.tf)
for the current set of policies/permissions for the `Deploy` IAM role.

In the [infra](https://gitlab.com/eric-dataflower/infra/) directory:

1. Run `AWS_PROFILE=DATAFLOWER_Main terraform init`
2. Run `AWS_PROFILE=DATAFLOWER_Main terraform apply`
3. Export Access Key & Secret to GitLab:
  1. Login to AWS.
  2. Go to the IAM page
  3. Click on `GitLab` user.
  4. [Click `Security Credentials` tab](https://console.aws.amazon.com/iam/home?region=ap-southeast-2#/users/GitLab?section=security_credentials).
  5. Click `Create access key`.
  6. Go to [GitLab CI/CD settings](https://gitlab.com/eric-dataflower/convert-currency/-/settings/ci_cd)
  7. Edit the `Variables` section and add the values for `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`
  8. <strong>Make sure the option for Masked is turned on</strong>.
  9. Add variable `AWS_DEFAULT_REGION`, setting it to `ap-southeast-2`.

This will create the `Deploy` IAM role, add access to the role to GitLab, and
enable the deployment of [src/convert](src/convert) on Terminal and on GitLab.

The above steps are run prior to the first [src/convert](src/convert)
deployment and when policies are adjusted for the deployment users and roles
in [infra/main.tf](https://gitlab.com/eric-dataflower/infra/-/blob/master/main.tf).

### Manual Deployment ###

* Run `pipenv shell` to gain access to `saml-login` command.

The following sections are to be run in order.

#### Infra ####

Follow instructions at [infra](https://gitlab.com/eric-dataflower/infra/-/blob/master/).

#### AppSync ####

In [src/appsync](src/appsync), run the following:

1. Authenticate using `saml-login -c DATAFLOWER_Main aws-assume-role`.
2. Select integration workspace using `AWS_PROFILE=DATAFLOWER_Main terraform workspace select int`.
3. Preview deployment changes using `AWS_PROFILE=DATAFLOWER_Main terraform plan`.
4. Deploy using `AWS_PROFILE=DATAFLOWER_Main terraform apply`.

To destroy the integration environment:

5. Select integration workspace using `AWS_PROFILE=DATAFLOWER_Main terraform workspace select int`.
6. To remove all AWS resources, run `AWS_PROFILE=DATAFLOWER_Main terraform destroy`.

#### Convert ####

Run the following, before repeating the above instructions in `src/convert`:

1. Make sure lambda dependencies are packaged:
  * In [src/convert/src](src/convert/src), run the following:
    - `make packages`
