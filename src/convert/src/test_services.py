import pytest
from services import ConvertCurrencyService, ThirdPartyAPIException
from connections import (
    AlphaVantageConnection,
    AlphaVantageCurrencyExchangeRateResponse,
    AlphaVantageResponse,
    AlphaVantageErrorResponse,
)
from currency import Currency
from decimal import Decimal


class OKAlphaVantageConnection(AlphaVantageConnection):
    """A mock `AlphaVantageConnection` returning an OK response."""

    def currency_exchange_rate(
        self, from_currency: Currency, to_currency: Currency
    ) -> AlphaVantageResponse:
        response: AlphaVantageCurrencyExchangeRateResponse = {
            "Realtime Currency Exchange Rate": {
                "1. From_Currency Code": from_currency,
                "2. From_Currency Name": from_currency,
                "3. To_Currency Code": to_currency,
                "4. To_Currency Name": to_currency,
                "5. Exchange Rate": "1.1",
                "6. Last Refreshed": "2020-01-01 00:00:00Z",
                "7. Time Zone": "UTC",
                "8. Bid Price": "1.1",
                "9. Ask Price": "1.1",
            }
        }
        return response


class ErrorAlphaVantageConnection(AlphaVantageConnection):
    """A mock `AlphaVantageConnection` returning an error response."""

    def currency_exchange_rate(
        self, from_currency: Currency, to_currency: Currency
    ) -> AlphaVantageResponse:
        response: AlphaVantageErrorResponse = {
            "Error Message": "ERROR_MESSAGE"
        }
        return response


def test_valid_call() -> None:
    service = ConvertCurrencyService(OKAlphaVantageConnection())
    assert service.get_exchange_rate("USD", "JPY") == Decimal("1.1")


def test_raises_api_exception() -> None:
    service = ConvertCurrencyService(ErrorAlphaVantageConnection())
    with pytest.raises(ThirdPartyAPIException):  # type: ignore
        service.get_exchange_rate("USD", "JPY")
