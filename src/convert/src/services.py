"""Interface to core business logic.

Read about service object at:
https://martinfowler.com/articles/refactoring-external-service.html
"""

import dataclasses
from decimal import Decimal
from typing import cast
from connections import (
    AlphaVantageConnection,
    AlphaVantageResponse,
    AlphaVantageCurrencyExchangeRateResponse,
    AlphaVantageErrorResponse,
)
from currency import Currency


class ThirdPartyAPIException(Exception):
    """Raised when an API call to the third party provider fails."""


@dataclasses.dataclass
class ConvertCurrencyService:
    """Service to get currency exchange rates."""

    alphavantage_connection: AlphaVantageConnection

    def get_exchange_rate(
        self, from_currency: Currency, to_currency: Currency
    ) -> Decimal:
        """Return the exchange rate of `from_currency` to `to_currency`."""
        response: AlphaVantageResponse = (
            self.alphavantage_connection.currency_exchange_rate(
                from_currency, to_currency
            )
        )

        if "Realtime Currency Exchange Rate" in response:
            data = cast(AlphaVantageCurrencyExchangeRateResponse, response)[
                "Realtime Currency Exchange Rate"
            ]
            rate = data["5. Exchange Rate"]
            return Decimal(rate)

        if "Error Message" in response:
            error = cast(AlphaVantageErrorResponse, response)["Error Message"]
            raise ThirdPartyAPIException(error)

        if "Note" in response:
            raise ThirdPartyAPIException(
                "Third party API call limit exceeded."
            )

        raise ThirdPartyAPIException(
            "Unrecognised response from third party API."
        )
