"""Connections to other sources of data.

Read about connection object at:
https://martinfowler.com/articles/refactoring-external-service.html
"""

import urllib.request
import dataclasses
import json
from typing import TypedDict, cast, Union
from currency import Currency


AlphaVantageCurrencyExchangeRate = TypedDict(
    "AlphaVantageCurrencyExchangeRate",
    {
        "1. From_Currency Code": str,
        "2. From_Currency Name": str,
        "3. To_Currency Code": str,
        "4. To_Currency Name": str,
        "5. Exchange Rate": str,
        "6. Last Refreshed": str,
        "7. Time Zone": str,
        "8. Bid Price": str,
        "9. Ask Price": str,
    },
)

AlphaVantageErrorResponse = TypedDict(
    "AlphaVantageErrorResponse", {"Error Message": str}
)

AlphaVantageNoteResponse = TypedDict("AlphaVantageNoteResponse", {"Note": str})
"""
Example:
{
    "Note": "Thank you for using Alpha Vantage! Our standard API call frequency is 5 calls per minute and 500 calls per day. Please visit https://www.alphavantage.co/premium/ if you would like to target a higher API call frequency."
}
"""

AlphaVantageCurrencyExchangeRateResponse = TypedDict(
    "AlphaVantageCurrencyExchangeRateResponse",
    {"Realtime Currency Exchange Rate": AlphaVantageCurrencyExchangeRate},
)

AlphaVantageResponse = Union[
    AlphaVantageCurrencyExchangeRateResponse,
    AlphaVantageErrorResponse,
    AlphaVantageNoteResponse,
]


ALPHAVANTAGE_HOST = "https://www.alphavantage.co"


@dataclasses.dataclass
class AlphaVantageConnection:
    """Connection to AlphaVantage API."""

    api_key: str = "demo"

    def currency_exchange_rate(
        self, from_currency: Currency, to_currency: Currency
    ) -> AlphaVantageResponse:
        """Returns the response of CURRENCY_EXCHANGE_RATE call to alphavantage.

        Example return:
        ```
        {
            "1. From_Currency Code": "USD",
            "2. From_Currency Name": "United States Dollar",
            "3. To_Currency Code": "JPY",
            "4. To_Currency Name": "Japanese Yen",
            "5. Exchange Rate": "107.92000000",
            "6. Last Refreshed": "2020-03-23 22:09:04",
            "7. Time Zone": "UTC",
            "8. Bid Price": "107.92000000",
            "9. Ask Price": "107.95000000",
        }
        ```
        """
        url = (
            f"{ALPHAVANTAGE_HOST}/query?function=CURRENCY_EXCHANGE_RATE"
            + f"&from_currency={from_currency}&to_currency={to_currency}"
            + f"&apikey={self.api_key}"
        )
        request = urllib.request.Request(url)
        response = urllib.request.urlopen(request)
        content = cast(str, response.read())
        return json.loads(content)  # type: ignore
