"""Test connections."""

from typing import cast
from connections import (
    AlphaVantageConnection,
    AlphaVantageCurrencyExchangeRate,
    AlphaVantageCurrencyExchangeRateResponse,
)


def test_currency_exchange_rate() -> None:
    """Test valid API call."""

    response = AlphaVantageConnection().currency_exchange_rate("USD", "JPY")
    assert "Realtime Currency Exchange Rate" in response

    exchange_rate: AlphaVantageCurrencyExchangeRate = cast(
        AlphaVantageCurrencyExchangeRateResponse, response
    )["Realtime Currency Exchange Rate"]

    assert (
        exchange_rate.keys()
        == AlphaVantageCurrencyExchangeRate.__annotations__.keys()  # type: ignore # pylint: disable=no-member
    )


def test_currency_exchange_rate_invalid_api_key() -> None:
    """Check error message for invalid API key."""
    response = AlphaVantageConnection(api_key="").currency_exchange_rate(
        "USD", "AUD"
    )

    assert response == {
        "Error Message": "the parameter apikey is invalid or missing. "
        "Please claim your free API key on "
        "(https://www.alphavantage.co/support/#api-key). It should take less "
        "than 20 seconds, and is free permanently."
    }


def test_currency_exchange_rate_invalid_currency() -> None:
    """Check error message for invalid currency."""
    response = AlphaVantageConnection(
        api_key="_invalid"
    ).currency_exchange_rate(
        "USD", "BAD_CURRENCY"  # type: ignore
    )

    assert response == {
        "Error Message": "Invalid API call. Please retry or visit the "
        "documentation (https://www.alphavantage.co/documentation/) for "
        "CURRENCY_EXCHANGE_RATE."
    }
