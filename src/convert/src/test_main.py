import pytest  # type: ignore
from main import handler, InvalidArgument


def test_appsync() -> None:
    data = handler(
        {
            "field": "getFXRate",
            "arguments": {"fromCurrency": "USD", "toCurrency": "JPY"},
        },
        None,
    )

    assert data
    assert "rate" in data


def test_invalid_from_currency() -> None:
    with pytest.raises(InvalidArgument):  # type: ignore
        handler(
            {
                "field": "getFXRate",
                "arguments": {
                    "fromCurrency": "NO_CURRENCY",  # type: ignore
                    "toCurrency": "JPY",
                },
            },
            None,
        )


def test_invalid_to_currency() -> None:
    with pytest.raises(InvalidArgument):  # type: ignore
        handler(
            {
                "field": "getFXRate",
                "arguments": {
                    "fromCurrency": "USD",
                    "toCurrency": "NO_CURRENCY",  # type: ignore
                },
            },
            None,
        )


def test_appsync_not_implemented_field() -> None:
    data = handler(
        {
            "field": "NO_SUCH_FIELD",  # type: ignore
            "arguments": {},  # type: ignore
        },
        None,
    )

    assert data is None
