"""Performs all external API calls for this project."""
import sys
import os
from typing import TypedDict, Optional

sys.path.insert(0, ".")
sys.path.insert(0, "site-packages")

from typing_extensions import Literal
from connections import AlphaVantageConnection
from currency import Currency
from services import ConvertCurrencyService

ALPHAVANTAGE_API_KEY = os.getenv("ALPHAVANTAGE_API_KEY", "demo")


Field = Literal["getFXRate"]


class GetFXRateArguments(TypedDict):
    """Arguments received from an getFXRate Appsync call."""

    fromCurrency: Currency
    toCurrency: Currency


class FXRate(TypedDict):
    rate: str


class InvalidArgument(Exception):
    "Raised when an argument passed through AppSync fails validation."


class Event(TypedDict):
    field: Field
    arguments: GetFXRateArguments


def handler(event: Event, _) -> Optional[FXRate]:  # type: ignore
    """Stub handler."""

    field = event["field"]
    arguments = event["arguments"]

    if field == "getFXRate":
        service = ConvertCurrencyService(
            AlphaVantageConnection(ALPHAVANTAGE_API_KEY)
        )

        if arguments["fromCurrency"] not in Currency.__args__:  # type: ignore
            raise InvalidArgument(
                f"fromCurrency {arguments['fromCurrency']} "
                "is not a valid currency."
            )

        if arguments["toCurrency"] not in Currency.__args__:  # type: ignore
            raise InvalidArgument(
                f"toCurrency {arguments['toCurrency']} "
                "is not a valid currency."
            )

        return {
            "rate": str(
                service.get_exchange_rate(
                    arguments["fromCurrency"], arguments["toCurrency"]
                )
            )
        }
    return None


if __name__ == "__main__":
    handler(
        {
            "field": "getFXRate",
            "arguments": {"fromCurrency": "USD", "toCurrency": "JPY"},
        },
        None,
    )
