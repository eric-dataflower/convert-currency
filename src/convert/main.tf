provider "aws" {
  region = "ap-southeast-2"
  assume_role {
    # 496027045990 is the sandbox account.
    role_arn = "arn:aws:sts::496027045990:role/Deploy"
  }
}

terraform {
  backend "s3" {
    bucket = "dataflower-terraform-state"
    key = "convert-currency.tfstate"
    region = "ap-southeast-2"
    encrypt = "true"
    dynamodb_table = "terraform-state-lock"
  }
}

data "aws_kms_secrets" "convert" {
  secret {
    name    = "alphavantage_api_key"
    payload = "AQICAHj0jUftF/VsCRbh/b6g4NcxXLqAUjtkMcUbMVLh9KN60QFiqUvZ4qcM6JT3pJyfkq2bAAAAbzBtBgkqhkiG9w0BBwagYDBeAgEAMFkGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMmBpF2AytXkq0UCzjAgEQgCxjUqESxPJgCSfjNquE05+uyOye6sgd4A0g6QFxZ5gWlGvv9kVs35yCAe/jGA=="
    context = {
      name = "alphavantage"
      type = "api"
    }
  }
}

resource "aws_kms_key" "convert_credentials" {
  description             = "${terraform.workspace}-convert-credentials"
  deletion_window_in_days = 10
}

output "aws_kms_key-convert_credentials" {
  value = aws_kms_key.convert_credentials
}

data "archive_file" "lambda" {
  type          = "zip"
  source_dir   = "src"
  output_path   = "lambda.zip"
}

resource "aws_iam_role" "convert_lambda_iam_role" {
  name = "${terraform.workspace}-convert_lambda_iam_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "convert" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "convert" {
  role = "${aws_iam_role.convert_lambda_iam_role.name}"
  policy_arn = "${aws_iam_policy.convert.arn}"
}

resource "aws_lambda_function" "convert" {
  filename  = "lambda.zip"
  function_name = "${terraform.workspace}-convert"
  role = aws_iam_role.convert_lambda_iam_role.arn
  handler = "main.handler"
  source_code_hash = data.archive_file.lambda.output_base64sha256
  runtime = "python3.8"
  environment {
    variables = {
      ALPHAVANTAGE_API_KEY = data.aws_kms_secrets.convert.plaintext["alphavantage_api_key"]
    }
  }
}

data "terraform_remote_state" "appsync" {
  backend = "s3"
  workspace = terraform.workspace
  config = {
    region = "ap-southeast-2"
    bucket = "dataflower-terraform-state"
    key = "appsync.tfstate"
    encrypt = "true"
  }
}

resource "aws_appsync_datasource" "convert" {
  api_id = data.terraform_remote_state.appsync.outputs.api.id
  name = "${terraform.workspace}_convert_lambda"
  service_role_arn = data.terraform_remote_state.appsync.outputs.role.arn
  type = "AWS_LAMBDA"
  lambda_config {
    function_arn = aws_lambda_function.convert.arn
  }
}

resource "aws_iam_role_policy" "appsync_to_convert" {
  name = "${terraform.workspace}-appsync_to_convert-lambda"
  role = data.terraform_remote_state.appsync.outputs.role.name
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Effect": "Allow",
      "Resource": "${aws_lambda_function.convert.arn}"
    }
  ]
}
EOF
}

resource "aws_appsync_resolver" "convert_getFXRate" {
  api_id = data.terraform_remote_state.appsync.outputs.api.id
  field = "getFXRate"
  type = "Query"
  data_source = aws_appsync_datasource.convert.name
  request_template = <<EOF
{
  "version": "2017-02-28",
  "operation": "Invoke",
  "payload": {
    "field": "getFXRate",
    "arguments": $utils.toJson($context.arguments)
  }
}
EOF
  response_template = "$util.toJson($context.result)"
}
