import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import React, { useState } from 'react';
import {
  IconButton,
  Grid,
  TextField,
  makeStyles,
  CircularProgress
} from '@material-ui/core';
import {
  Autocomplete
} from '@material-ui/lab';
import SwapHorizIcon from '@material-ui/icons/SwapHoriz';

export const GET_FX_RATE = gql`
  query getFXRate ($fromCurrency: String!, $toCurrency: String!){
    getFXRate(fromCurrency: $fromCurrency, toCurrency: $toCurrency) {
      rate
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  grid: {
    marginTop: 'auto',
    paddingBottom: theme.spacing(24),
    minHeight: "100vh"
  },
}));

const currencyOptions = [
  "AED",
  "AFN",
  "ALL",
  "AMD",
  "ANG",
  "AOA",
  "ARS",
  "AUD",
  "AWG",
  "AZN",
  "BAM",
  "BBD",
  "BDT",
  "BGN",
  "BHD",
  "BIF",
  "BMD",
  "BND",
  "BOB",
  "BRL",
  "BSD",
  "BTN",
  "BWP",
  "BZD",
  "CAD",
  "CDF",
  "CHF",
  "CLF",
  "CLP",
  "CNH",
  "CNY",
  "COP",
  "CUP",
  "CVE",
  "CZK",
  "DJF",
  "DKK",
  "DOP",
  "DZD",
  "EGP",
  "ERN",
  "ETB",
  "EUR",
  "FJD",
  "FKP",
  "GBP",
  "GEL",
  "GHS",
  "GIP",
  "GMD",
  "GNF",
  "GTQ",
  "GYD",
  "HKD",
  "HNL",
  "HRK",
  "HTG",
  "HUF",
  "IDR",
  "ILS",
  "INR",
  "IQD",
  "IRR",
  "ISK",
  "JEP",
  "JMD",
  "JOD",
  "JPY",
  "KES",
  "KGS",
  "KHR",
  "KMF",
  "KPW",
  "KRW",
  "KWD",
  "KYD",
  "KZT",
  "LAK",
  "LBP",
  "LKR",
  "LRD",
  "LSL",
  "LYD",
  "MAD",
  "MDL",
  "MGA",
  "MKD",
  "MMK",
  "MNT",
  "MOP",
  "MRO",
  "MRU",
  "MUR",
  "MVR",
  "MWK",
  "MXN",
  "MYR",
  "MZN",
  "NAD",
  "NGN",
  "NOK",
  "NPR",
  "NZD",
  "OMR",
  "PAB",
  "PEN",
  "PGK",
  "PHP",
  "PKR",
  "PLN",
  "PYG",
  "QAR",
  "RON",
  "RSD",
  "RUB",
  "RUR",
  "RWF",
  "SAR",
  "SBD",
  "SCR",
  "SDG",
  "SEK",
  "SGD",
  "SHP",
  "SLL",
  "SOS",
  "SRD",
  "SYP",
  "SZL",
  "THB",
  "TJS",
  "TMT",
  "TND",
  "TOP",
  "TRY",
  "TTD",
  "TWD",
  "TZS",
  "UAH",
  "UGX",
  "USD",
  "UYU",
  "UZS",
  "VND",
  "VUV",
  "WST",
  "XAF",
  "XAG",
  "XAU",
  "XCD",
  "XDR",
  "XOF",
  "XPF",
  "YER",
  "ZAR",
  "ZMW",
  "ZWL",
]


function App() {
  const classes = useStyles();

  const [fromCurrency, setFromCurrency] = useState("AUD")
  const [toCurrency, setToCurrency] = useState("USD")
  const [fromAmount, setFromAmount] = useState<number | null>(1)

  let fxRate: number | null = null
  let fxRateError: string = ""
  let toAmount: number | null = null

  const { loading, error, data } = useQuery(GET_FX_RATE, {
    variables: {
      fromCurrency: fromCurrency,
      toCurrency: toCurrency
    }
  });

  if (!loading && !error) {
    fxRate = data.getFXRate.rate
    if (fxRate && fromAmount) {
      toAmount = fxRate * fromAmount
    }
  }
  else if (error) {
    fxRateError = error.message
  }

  const onClick = () => {
    setToCurrency(fromCurrency)
    setFromCurrency(toCurrency)
  }

  return (
    <div className="App">
      <main className={classes.layout}>
        <Grid container spacing={1}
          className={classes.grid}
          alignItems='center'
          justify="center"
          direction="row">
          <Grid item xs={1}>
            <IconButton color="primary" size="small" onClick={onClick}>
              <SwapHorizIcon />
            </IconButton>
          </Grid>
          <Grid item xs={11}>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <Autocomplete
                  id="fromCurrency"
                  options={currencyOptions}
                  getOptionLabel={option => option}
                  value={fromCurrency}
                  onChange={(event: React.ChangeEvent<{}>, value: string | null) => {
                    if (value !== null) {
                      setFromCurrency(value)
                    }
                  }}
                  renderInput={params => <TextField {...params}
                    label="From Currency" variant="outlined" />}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Autocomplete
                  id="toCurrency"
                  options={currencyOptions}
                  getOptionLabel={option => option}
                  value={toCurrency}
                  onChange={(event: React.ChangeEvent<{}>, value: string | null) => {
                    if (value !== null) {
                      setToCurrency(value)
                    }
                  }}
                  renderInput={params => <TextField {...params}
                    label="To Currency" variant="outlined" />}
                />
              </Grid>
            </Grid>
            <Grid container spacing={3}
              alignItems='center'
              justify="center"
              direction="row">
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  name="amount"
                  label="From Amount"
                  fullWidth
                  autoComplete="lname"
                  variant="filled"
                  value={fromAmount !== null ? fromAmount : ""}
                  type="number"
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    if (event.target) {
                      const value = parseFloat(event.target.value)
                      if (value >= 0) {
                        return setFromAmount(value)
                      }
                    }
                    setFromAmount(null)
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6} style={{ textAlign: 'center' }}>
                {!loading ? <TextField
                  label="To Amount"
                  id="toAmount"
                  fullWidth
                  autoComplete="lname"
                  variant="filled"
                  style={loading ? { opacity: 0 } : {}}
                  InputProps={{
                    readOnly: true,
                  }}
                  value={toAmount !== null ? toAmount : fxRateError}
                /> : <CircularProgress size={24} variant={"indeterminate"} />}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </main>
    </div>
  );
}

export default App;
