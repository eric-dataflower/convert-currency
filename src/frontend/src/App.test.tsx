import React from 'react';
import ReactDOM from 'react-dom';
import { wait } from "@testing-library/react";
import { act } from 'react-dom/test-utils';
import { GET_FX_RATE } from './App';
import App from './App'
import { MockedProvider } from '@apollo/react-testing';


const mocks = [
  {
    request: {
      query: GET_FX_RATE,
      variables: {
        fromCurrency: 'AUD',
        toCurrency: 'JPY',
      },
    },
    result: {
      data: {
        getFXRate: { rate: '100' },
      },
    },
  },
];

let container: Element | null;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = null;
});

test('renders app', async () => {

  act(() => {
    ReactDOM.render(<MockedProvider mocks={mocks} addTypename={false}>
      <App />
    </MockedProvider>, container)
  })
  await act(wait);
});
