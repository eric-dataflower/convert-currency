# Usage: AWS_PROFILE=DATAFLOWER_Main bash dotenv_development.sh
# This updates .env.development to match the int environment.

_=$(cd ../appsync && AWS_PROFILE=DATAFLOWER_Main terraform workspace select int)

printf \
REACT_APP_APPSYNC_API_KEY=$(cd ../appsync && AWS_PROFILE=DATAFLOWER_Main terraform output -json -no-color | \
  node -e 'const fs = require("fs"); console.log(JSON.parse(fs.readFileSync(0, "utf-8")).key.value.key)') > .env.development
printf "\n" >> .env.development
printf \
REACT_APP_APPSYNC_API_URI=$(cd ../appsync && AWS_PROFILE=DATAFLOWER_Main terraform output -json -no-color | \
  node -e 'const fs = require("fs"); console.log(JSON.parse(fs.readFileSync(0, "utf-8")).api.value.uris.GRAPHQL)') >> .env.development
