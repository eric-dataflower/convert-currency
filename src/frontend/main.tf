provider "aws" {
  region = "ap-southeast-2"
  assume_role {
    # 496027045990 is the sandbox account.
    role_arn = "arn:aws:sts::496027045990:role/Deploy"
  }
}

terraform {
  backend "s3" {
    bucket = "dataflower-terraform-state"
    key = "frontend.tfstate"
    region = "ap-southeast-2"
    encrypt = "true"
    dynamodb_table = "terraform-state-lock"
  }
}

resource "aws_s3_bucket" "frontend" {
  bucket = "fx.${terraform.workspace}.dataflower.com.au"
  acl = "public-read"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT","POST"]
    allowed_origins = ["*"]
    expose_headers = ["ETag"]
    max_age_seconds = 3000
  }

  policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "PublicReadForGetBucketObjects",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::fx.${terraform.workspace}.dataflower.com.au/*"
    },
    {
      "Sid": "PublicReadGetObject",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::fx.${terraform.workspace}.dataflower.com.au/*",
      "Condition": {
        "IpAddress": {
          "aws:SourceIp": [
              "2400:cb00::/32",
              "2405:8100::/32",
              "2405:b500::/32",
              "2606:4700::/32",
              "2803:f800::/32",
              "2c0f:f248::/32",
              "2a06:98c0::/29",
              "103.21.244.0/22",
              "103.22.200.0/22",
              "103.31.4.0/22",
              "104.16.0.0/12",
              "108.162.192.0/18",
              "131.0.72.0/22",
              "141.101.64.0/18",
              "162.158.0.0/15",
              "172.64.0.0/13",
              "173.245.48.0/20",
              "188.114.96.0/20",
              "190.93.240.0/20",
              "197.234.240.0/22",
              "198.41.128.0/17"
          ]
        }
      }
    }
  ]
}
EOF
  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}


resource "aws_s3_bucket_object" "test" {
  for_each = fileset(path.module, "build/**/*")

  bucket = aws_s3_bucket.frontend.bucket
  key    = trimprefix(each.value, "build/")
  content_type = lookup({
    ".txt" = "text/plain; charset=utf-8"
    ".html" = "text/html; charset=utf-8"
    ".png" = "image/png"
    ".json" = "application/json"
    ".js" = "application/javascript"
    ".map" = "application/octet-stream"
  }, regex("\\.[^.]+$", each.value), "binary/octet-stream")
  source = "${path.module}/${each.value}"
}
