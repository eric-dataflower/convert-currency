provider "aws" {
  region = "ap-southeast-2"
  assume_role {
    # 496027045990 is the sandbox account.
    role_arn = "arn:aws:sts::496027045990:role/Deploy"
  }
}

terraform {
  backend "s3" {
    bucket = "dataflower-terraform-state"
    key = "appsync.tfstate"
    region = "ap-southeast-2"
    encrypt = "true"
    dynamodb_table = "terraform-state-lock"
  }
}

resource "aws_iam_role" "appsync" {
  name = "${terraform.workspace}-appsync"

  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
        "Effect": "Allow",
        "Principal": {
            "Service": "appsync.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
        }
    ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "appsync" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSAppSyncPushToCloudWatchLogs"
  role       = aws_iam_role.appsync.name
}

resource "aws_appsync_graphql_api" "appsync" {
  authentication_type = "API_KEY"
  name = "${terraform.workspace}-appsync"
  schema = file("schema.graphql")

  log_config {
    cloudwatch_logs_role_arn = "${aws_iam_role.appsync.arn}"
    field_log_level          = "ALL"
  }
}

resource "aws_appsync_api_key" "appsync" {
  api_id  = "${aws_appsync_graphql_api.appsync.id}"
  expires = timeadd(timestamp(), "8759h")
}

resource "aws_appsync_datasource" "appsync_none" {
  api_id = "${aws_appsync_graphql_api.appsync.id}"
  name = "${terraform.workspace}_none"
  type = "NONE"
}

output "key" {
  value = aws_appsync_api_key.appsync
}

output "api" {
  value = aws_appsync_graphql_api.appsync
}

output "role" {
  value = aws_iam_role.appsync
}
